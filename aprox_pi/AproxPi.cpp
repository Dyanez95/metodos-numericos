#include <cmath>
#include "AproxPi.h"

// pi^2/6=sum from 1 to infinity of 1/n^2
double aproxPiEuler(double en){
	double pi, piOld, enCurrent;

	piOld = 1.;
	pi = piOld;
	enCurrent = 100.;

	int i = 2;

	while (enCurrent > en){
		pi = pi + pow((double)i, -2.);
		enCurrent = (fabs(pi - piOld))*100. / pi;
		piOld = pi;
		i++;
	}

	pi *= 6.;
	pi = sqrt(pi);

	return pi;
}

// pi=4(sum from 0 to infinity of (-1)^i/(2n+1))
double aproxPiLeib(double en){
	double pi, piOld, enCurrent;
	piOld = 1.;
	pi = piOld;
	enCurrent = 100.;

	int i = 1;

	while (enCurrent > en){
		pi = pi + (pow(-1.0, (double)i)) / ((double)(2*i+1));
		enCurrent = fabs((pi - piOld)*100. / pi);
		piOld = pi;
		i++;
	}
	pi=pi*4.;
	return pi;
}

double aproxPiBroun(double en){
	double pi, piOld, enCurrent;
	piOld = 1.;
	pi = piOld;
	enCurrent = 100.;

	int i;

	i = 1;

	while (enCurrent > en){

		i += 2;

		pi = pi + pow((double)i, -2.);

		enCurrent = fabs((pi - piOld)*100. / pi);

		piOld = pi;

	}

	pi *= 8.;

	pi = sqrt(pi);

	return pi;
}