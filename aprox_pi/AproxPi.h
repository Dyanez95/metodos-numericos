#ifndef __APROX_PI__
#define __APROX_PI__

double aproxPiEuler(double en);
double aproxPiLeib(double en);
double aproxPiBroun(double en);

#endif