#include <iostream>
#include "AproxPi.h"

using namespace std;

int main(void){

	double pi, en;

	en = 0.00001;

	pi = aproxPiLeib(en);

	cout << "Pi = " << pi << " with normalized error en = " << en << " using Leibniz's formula\n";

	pi = aproxPiEuler(en);

	cout << "Pi = " << pi << " with normalized error en =" << en << " using Euler's formula for pi\n";

	pi = aproxPiBroun(en);

	cout << "Pi = " << pi << " with normalized error en =" << en << " using Euler's formula for pi\n";

	return 0;
}