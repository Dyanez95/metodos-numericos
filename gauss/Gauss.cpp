#include <fstream>
#include <string>
#include <iostream>
#include <cmath>
#include <cstdlib>

using namespace std;

//exchanges two arrays of doubles
void exchange_rows(double **row1, double **row2);
//multiplies all elements of array row by the num
//returns result in a new array
double* multiply_row_by(double *row, double num, int cols);
//sums arrays element by element
//returns result in a new array
double * sum_rows(double *row1, double *row2, int cols);
//returns 0 if consistent system
//1 if infinite solutions
//2 if inconsistent
//creates expanded matrix based on passed matrix and solution vector
//if the system is consistent and with unique solution vector, the solution is stored in passed solution array
int solveByGaussJordan(double **ab, int n, double *&solution, double &det);

//gets the determinant of the square matrix a
//using gauss-jordan method
double getDeterminant(double **a, int n);

//obtains the inverse of a using gauss-jordan method
//result is stored int inverse
bool getMatrixInverse(double **a, int n, double **&inverse);

//reads a file where the text represents a system of linear equations
//coefficients are separated by commas
//resulting matrix is stored in ab and the number of equations is stored in n
int readLinearEquationSystemFromFile(string fileName, double **&ab, int &n);

//reads a file where the text represents a square matrix
//elements of the matrix are separated by commas
//resulting matrix is stored in matrix, and its dimensions are stored in n
int readMatrixFromFile(string fileName, double **&matrix, int &n);

//prints solution of a system of linear equations using gauss-jordan method, or a message indicating that
//the system is inconsistent
void printSolutionSystemOfEq(double **ab, int n);

//prints the inverse of matrix using gauss-jordan method, or a message indicating that 
//the matrix is singular
void printSolutionInverse(double **matrix, int n);

void exchange_rows(double **row1, double **row2){

	double *tmp = *row1;

	*row1 = *row2;

	*row2 = tmp;

}

double*multiply_row_by(double *row, double num, int cols){
	double *new_array = new double[cols];

	for (int i = 0; i < cols; i++){
		new_array[i] = row[i] * num;
	}

	return new_array;
}

double*sum_rows(double *row1, double *row2, int cols){
	double *result = new double[cols];

	for (int i = 0; i < cols; i++){
		result[i] = row1[i] + row2[i];
	}

	return result;
}

int solveByGaussJordan(double **ab, int n,double *&solution, double &det){



	double piv;
	int col=0;

	//iterate rows, obtaining pivots in the main diagonal
	for (int i = 0; i < n; i++){

		piv = ab[i][col];

		//selected pivot is too close to zero
		//select new pivot from rows below current one
		if (fabs(piv) < 0.00001){

			//if the zero pivot is the last one
			//choosing a non-zero pivot is not possible
			if (i == (n - 1)){
				det = 0.;

				if (fabs(ab[i][n]) < 0.00001){
					return 1;
				}
				else{
					return 2;
				}
			}

			for (int j = i+1; j < n; j++){
				//check if potential new pivot is not too close to zero
				if (!(fabs(ab[j][col]) < 0.00001)){

					//we've found a non-zero pivot, exchange its row with the one we were using before
					//and asign new value of pivot

					exchange_rows(ab + i, ab + j);
					piv = ab[i][col];
					break;
				}
				//this happens when choosing a new non-zero pivot is not possible
				//system is therefore without an unique solution
				if (j == (n - 1)){

					det = 0.;

					if (fabs(ab[i][n]) < 0.00001){
						return 1;
					}
					else{
						return 2;
					}
				}
			}
		}

		if (col == 0){
			det = piv;
		}
		else{
			det *= piv;
		}

		//we must now turn pivot into one
		//multiply its row by its multiplicative inverse

		double *tmp=NULL;

		if(!(fabs(piv)<1.000001)){
			tmp=ab[i];

			ab[i]=multiply_row_by(ab[i], pow(piv, -1.), n+1);

			//the row of the pivot is now multiplied by the inverse of the pivot, must release old row

			delete[] tmp;

			tmp = NULL;
		}


		double *tmp2=NULL;

		//iterate over rows to create a column of zeros in the column of the pivot
		for (int j = 0; j < n; j++){
			//avoid the row of the pivot
			if (j != i){

				if(!(fabs(ab[j][col])<0.00001)){
					tmp = multiply_row_by(ab[i], ab[j][col] * -1., n + 1);
	
					tmp2 = sum_rows(ab[j], tmp, n + 1);

					exchange_rows(&tmp2, ab + j);

					//this created a zero  in column of the pivot in row j

					delete[] tmp2;
					delete[] tmp;

					tmp = tmp2 = NULL;

					//delete temporal arrays to hold basic row operations
				}

			}
		}

		//increase current column
		col++;
	}

	//if we get out of the loop, we have a consistent system with unique solution

	//delete expanded matrix
	for (int i = 0; i < n; i++){
		solution[i] = ab[i][n];
	}

	return 0;
}

double getDeterminant(double **a, int n){
	double **dummy_expanded_matrix = new double*[n];

	for (int i = 0; i < n; i++){
		dummy_expanded_matrix[i] = new double[n + 1];
		for (int j = 0; j <n ; j++){
			dummy_expanded_matrix[i][j] = a[i][j];
		}
		dummy_expanded_matrix[i][n] = 0.;
	}

	double det;

	double *solution = new double[n];

	solveByGaussJordan(dummy_expanded_matrix, n, solution, det);

	delete[] solution;
	
	for (int i = 0; i < n; i++){
		delete[] dummy_expanded_matrix[i];
	}

	delete[] dummy_expanded_matrix;

	return det;
}

bool getMatrixInverse(double **a, int n, double **&inverse){
	double **ab = new double*[n];


	for (int i = 0; i < n; i++){
		ab[i] = new double[2*n];
		for (int j = 0; j < (2 * n); j++){
			if (j < n){
				ab[i][j] = a[i][j];
			}
			else{

				if (j==(n+i)){
					ab[i][j] = 1.;
				}
				else{
					ab[i][j] = 0.;
				}
			}
		}
	}

	double piv;
	int col = 0;


	for (int i = 0; i < n; i++){
		piv = ab[i][col];

		if (fabs(piv) < 0.00001){
			if (i == (n - 1)){

				for (int j = 0; j < n; j++){
					delete[] ab[i];
				}

				delete[] ab;

				return false;
			}

			for (int j = i + 1; j < n; j++){
				if (!(fabs(ab[j][col]) < 0.00001)){

					exchange_rows(ab + i, ab + j);
					piv = ab[i][col];
					break;

				}

				if (j == (n - 1)){

					for (int j = 0; j < n; j++){
						delete[] ab[i];
					}

					delete[] ab;

					return false;
				}
			}
		}

		double *tmp=NULL;

		if(!(fabs(piv)<1.000001)){
			tmp = ab[i];

			ab[i] = multiply_row_by(ab[i], pow(piv, -1.), 2*n);

			delete[] tmp;

			tmp = NULL;

		}
		
		double *tmp2 = NULL;

		for (int j = 0; j < n; j++){
			if (j != i){
				if(!(fabs(ab[j][col])<0.00001)){
					tmp = multiply_row_by(ab[i], ab[j][col] * -1., 2*n);

					tmp2 = sum_rows(ab[j], tmp, 2*n);

					exchange_rows(&tmp2, ab + j);

					delete[] tmp2;
					delete[] tmp;

					tmp = tmp2 = NULL;
				}
			}
		}

		col++;
	}

	for (int i = 0; i < n; i++){
		for (int j = n; j < (2 * n); j++){
			inverse[i][j - n] = ab[i][j];
		}
		delete[] ab[i];
	}

	delete[] ab;

	return true;
}

int readLinearEquationSystemFromFile(string fileName, double **&ab, int &n){

	fstream file;
	file.open(fileName);

	string line;
	string tmp;
	char cur_char;

	int row, col;

	row = col = 0;
	n = 0;

	if (file.is_open()){
		while (!file.eof()){
			getline(file, line);

			string::iterator i;

			for (i = line.begin(); i != line.end(); ++i){
				cur_char = *i;

				

				if (cur_char == ',' || cur_char == ' '){
					ab[row][col] = atof(tmp.c_str());

					tmp.clear();
					col++;
				}
				else{
					tmp.append(1,cur_char);
				}
			}

			ab[row][col] = atof(tmp.c_str());

			tmp.clear();
		
			row++;
			n++;
			col = 0;

		}

		file.close();

		return 0;
	}

	return 1;
}

int readMatrixFromFile(string fileName, double **&matrix, int &n){
	fstream file;
	file.open(fileName);

	string line;
	string tmp;
	char cur_char;

	int row, col;

	row = col = 0;
	n = 0;

	if (file.is_open()){
		while (!file.eof()){
			getline(file, line);

			string::iterator i;

			for (i = line.begin(); i != line.end(); ++i){
				cur_char = *i;

				if (cur_char == ','){
					matrix[row][col] = atof(tmp.c_str());
					
					col++;
					tmp.clear();
				}
				else{
					tmp.append(1, cur_char);
				}
			}

			matrix[row][col] = atof(tmp.c_str());

			tmp.clear();

			row++;
			col = 0;
			n++;
		}

		file.close();

		return 0;
	}

	return 1;
}


void printSolutionSystemOfEq(double **ab, int n){

	int code;
	double det;
	double *solution_gauss = new double[n];

	code = solveByGaussJordan(ab, n, solution_gauss, det);

	if (code == 0){
		cout << "solved using gauss jordan method\n";
		for (int i = 0; i < n; i++){
			cout << "b" << i <<" = "<< solution_gauss[i] << "\n";
		}
		cout << "det = "<<det << "\n";
	}
	else if (code == 1){
		cout << "system has an infinite number of solutions\n";
	}
	else if (code == 2){
		cout << "system has no solution\n";
	}
	delete[] solution_gauss;
}

void printSolutionInverse(double **matrix, int n){
	
	double **inverse = new double*[n];

	for (int i = 0; i < n; i++){
		inverse[i] = new double[n];
	}

	bool nonsingular = getMatrixInverse(matrix, n, inverse);

	if (nonsingular){
		cout << "matrix is nonsingular, inverse obtained using gauss jordan method\n";
		for (int i = 0; i < n; i++){
			for (int j = 0; j < n; j++){
				cout << "[" << inverse[i][j] << "] ";
			}
			cout << "\n";
		}
	}
	else{
		cout << "matrix is singular, inverse does not exist\n";
	}

	for (int i = 0; i < n; i++){
		delete[] inverse[i];
	}

	delete[] inverse;
}

int main(void){

	string file1 = "test.txt";
	string file2 = "test2.txt";

	double **matrix1 = new double*[3];
	double **matrix2 = new double*[3];

	for (int i = 0; i < 3; i++){
		matrix1[i] = new double[4];
		matrix2[i] = new double[3];
	}

	int n1, n2;

	double det;

	int code1 = readLinearEquationSystemFromFile(file1, matrix1, n1);

	if (code1 == 0){
		printSolutionSystemOfEq(matrix1, n1);
	}

	int code2 = readMatrixFromFile(file2, matrix2, n2);

	if (code2 == 0){
		printSolutionInverse(matrix2, n2);
	}

	for (int i = 0; i < 3; i++){
		delete[] matrix1[i];
		delete[] matrix2[i];
	}

	delete[] matrix1;
	delete[] matrix2;

	getchar();

	return 0;
}