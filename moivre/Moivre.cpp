#include <cmath>
#include <cstdlib>
#include "Moivre.h"

//calculate nth unit complex roots
complex**moivre_unit_root(int n){
	complex**roots = (complex**)calloc(n, sizeof(complex*));

	double real, imaginary;

	for (int i = 0; i < n; i++){
		real = cos((double)(2.*PI*i/n));
		imaginary = sin((double)(2.*PI*i/n));

		//numbers too close to zero are asigned zero instead
		if (fabs(imaginary) < 1.e-006){
			imaginary = 0.;
		}

		if (fabs(real) < 1.e-006){
			real = 0.;
		}

		roots[i] = (complex*)malloc(sizeof(complex));
		roots[i]->real = real;
		roots[i]->imaginary = imaginary;
	}

	return roots;

}