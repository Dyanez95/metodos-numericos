#ifndef __MOIVRE__
#define __MOIVRE__
#define PI 3.141592 
struct complex_s{
	double real, imaginary;
};

typedef complex_s complex;

complex**moivre_unit_root(int n);

#endif