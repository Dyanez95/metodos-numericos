#include <iostream>
#include "Moivre.h"
#include <cstdlib>

using namespace std;

int main(void){

	int n;

	complex**roots;

	cout << "Calculate nth unit complex root using D'Moivre theorem\n";

	do{
		cout << "Enter a natural number, enter zero to stop\n";
		cin >> n;
		roots = moivre_unit_root(n);

		for (int i = 0; i < n; i++){
			cout << "x" << i << " = " << roots[i]->real;
			if (roots[i]->imaginary >= 0.){
				cout << "+";
			}
			cout << roots[i]->imaginary << "i\n";
			free(roots[i]); //free each complex number after reading its info
			roots[i] = NULL;
		}
		free(roots);// free whole matrix of complex numbers
		roots = NULL;
		
	} while (n > 0);

	return 0;
}