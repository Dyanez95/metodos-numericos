#include <cmath>
#include <iostream>
#include <string>

#define ITERMAX 10000
#define ZERO 0.000001

using namespace std;

//parametros de entrada
//a,b: limites inferiores y superiores donde se encuentra una raiz unica
//function: funcion a la que se quiere obtener una raiz en el intervalo [a,b], debe recibir un numero real y regresar un numero real
//en: error relativo normalizado que indica la precision de la funcion
//parametros de salida
//root: raiz a la que se convergio con el metodo
//enCurrent: error relativo normalizado con el que se consiguio la raiz
//iter: numero de iteraciones que se necesitaron para converger a la raiz
//regresa 1 si se convergio a una raiz, -1 si se falla converger a una raiz en menos iteraciones que ITERMAX
int solveByBisection(double a, double b, double(*function)(double),double en, double &root, double &enCurrent, int &iter);
int solveBySecant(double a, double b, double(*function)(double),double en, double &root, double &enCurrent, int &iter);

//imprime raiz con formato
//parametros de entrada
//name: nombre del metodo
//code: codigo de que paso con el metodo usado, vease arriba
//root: raiz encontrada por el metodo
//enCurrent: error relativo normalizado con el que se convergio a la raiz
//iter: numero de iteraciones
void printRoot(string name,int code, double root, double enCurrent,int iter);

int solveByBisection(double a, double b, double(*function)(double), double en, double &root, double &enCurrent, int &iter){
	double mean, rootOld,meanFuncValue;

	mean = (a + b) / 2.;
	meanFuncValue = function(mean);

	rootOld = mean;
	root = rootOld;

	iter = 0;
	enCurrent = 100.;

	while (enCurrent > en){
		if (iter > ITERMAX){
			return -1;
		}
		if (function(a)*meanFuncValue < 0.){
			b = mean;
		}
		else if (function(b)*meanFuncValue < 0.){
			a = mean;
		}
		mean = (a + b) / 2.;
		meanFuncValue = function(mean);
		root = mean;
		enCurrent = fabs((root - rootOld) / root*100.);
		rootOld = root;
		iter++;
	}

	return 1;
}

int solveBySecant(double a, double b, double(*function)(double), double en, double &root, double &enCurrent, int &iter){
	double secant, rootOld, secantFuncValue;

	secant = a - (function(a)*(b-a))/(function(b)-function(a));

	secantFuncValue = function(a);

	enCurrent = 100.;

	iter = 0;

	rootOld = secant;
	root = rootOld;

	while (enCurrent > en){
		if (iter > ITERMAX){
			return -1;
		}
		if (function(a)*secantFuncValue < 0.){
			b = secant;
		}
		else if (function(b)*secantFuncValue < 0.){
			a = secant;
		}

		secant = a - (function(a)*(b - a)) / (function(b) - function(a));
		secantFuncValue = function(a);

		root = secant;

		enCurrent = fabs((root - rootOld) / root*100.);

		rootOld = root;

		iter++;
	}
	return 1;
}


void printRoot(string name, int code, double root, double enCurrent, int iter){
	cout << "Solved with: "<<name << "\n";
	if (code == 1){
		cout << "x = " << root << " with en = " << enCurrent << " with iterations: " << iter << "\n";
	}
	else if (code == -1){
		cout << "failed to converge with iterations: " << iter << "\n";
	}
}

double test_sine(double x){
	return 2.*sin(x) - x;
}

int main(void){

	double a,b,en, root, enCurrent;
	int iter,code;
	a = 0.1;
	b = 3.;
	en = 0.00001;

	code = solveByBisection(a, b, &test_sine, en, root, enCurrent, iter);

	printRoot("bisection", code, root, enCurrent, iter);

	a = 0.1;
	b = 3.;

	code = solveBySecant(a, b, &test_sine, en, root, enCurrent, iter);

	cout << "\n";

	printRoot("secant", code, root, enCurrent, iter);
	return 0;
}