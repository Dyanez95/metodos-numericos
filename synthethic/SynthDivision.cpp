#include <cmath>
#include <iostream>
#include <vector>

using namespace std;

//obtain natural factors of passed value, incluiding 1 and itself, must be nonnegative
vector<int>*get_factors(int);

//checks if a number is in vector, used to avoid repeated possible roots
bool contains(vector<double>, double);

//obtain possible rational roots of a polynomial equation, passing factors of lowest and highest degree coefficient
vector<double>*get_possible_roots(vector<int>*, vector<int>*);

//perform synthetic division method, array of integers are the n coefficients of a nth degree polynomial, from largest degree to independent term
//results are stored in passed pointer to vector of doubles
void synth_division(int*,int,vector<double>*);

//print rational roots of a nth degree polynomial obtained from synthethic division
void print_solution(vector<double>*);

vector<int>*get_factors(int x){
	vector<int> *fact = new vector<int>();

	int mod;

	for (int i = 1; i <= x; i++){
		mod = x%i;

		if (mod == 0){
			fact->push_back(i);
		}
	}
	return fact;
}

bool contains(vector<double> *v, double x){
	vector<double>::const_iterator i;

	for (i = v->begin(); i != v->end(); ++i){
		if ((*i) == x){
			return true;
		}
	}

	return false;
}

vector<double> *get_possible_roots(vector<int> *fact_largest_degree, vector<int> *fact_lowest_degree){
	vector<double> *possible_roots = new vector < double > ();
	vector<int>::const_iterator i, j;

	double cur;

	for (i = fact_lowest_degree->begin(); i != fact_lowest_degree->end(); ++i){
		for (j = fact_largest_degree->begin(); j != fact_largest_degree->end(); ++j){
			cur = (double)(*i)/(*j);
			if (!contains(possible_roots, cur)){
				possible_roots->push_back(cur);
				possible_roots->push_back(cur*-1.);
			}
		}
	}

	return possible_roots;
}

void synth_division(int *coefficients, int degree, vector<double> *roots){

	for (int j = 0; j <= degree; j++){
		if (coefficients[degree] == 0){
			for (int i = 0; i < degree; i++){
				coefficients[i + 1] = coefficients[i];
			}
			roots->push_back(0.);

			coefficients++;
			degree--;
		}
	}

	vector<int> *facts_largest = get_factors((int)fabs((double)coefficients[0]));
	vector<int> *facts_lowest = get_factors((int)fabs((double)coefficients[degree]));

	vector<double> *possible_roots = get_possible_roots(facts_largest, facts_lowest);

	vector<double>::const_iterator i;

	for (i = possible_roots->begin(); i != possible_roots->end(); ++i){
		double cur = (double)coefficients[0];
		for (int j = 1; j <= degree; j++){
			cur *= (*i);
			cur += coefficients[j];
		}
		if (cur == 0.){
			roots->push_back(*i);
		}
	}

	facts_largest->~vector();
	facts_lowest->~vector();
	possible_roots->~vector();

}

void print_solution(vector<double> *roots){
	vector<double>::const_iterator i;
	int x = 0;
	for (i = roots->begin(); i != roots->end(); ++i){
		cout << "x" << x << "= " << (*i) << "\n";
		x++;
	}
}

int main(void){

	int degree;

	cout << "Enter degree of polynomial\n";

	cin >> degree;

	int *polynomial = new int[degree+1];

	cout << "Enter coefficients of polynomial\n";

	for (int i = 0; i <= degree; i++){
		cout << "a" << degree - i<<"=\n";
		cin >> polynomial[i];
	}

	cout << "These are the rational roots of the polynomial\n";

	vector<double> *roots = new vector<double>();

	synth_division(polynomial, degree, roots);

	print_solution(roots);

	return 0;
}