#include <iostream>
#include <cmath>
#include <string>
#include <ctime>

using namespace std;

/*
Author:Diego Yanez
*/

//unidimensional representation of a matrix using row major order
//creates random matrix of mxn order
double*createMatrix(int m, int n);
//sum two matrices
double*sumMatrix(int m, int n, double* a, double *b);
//obtain matrix product, if it exists
double*prodMatrix(int m1, int n1, int m2, int n2, double *a, double *b);
//return string representation of a matrix
string matrixToString(int m, int n, double *a);
//print a matrix with format
void printMatrix(string title, int m, int n, double *a);

double*createMatrix(int m, int n){
	double *a = new double[m*n];

	for (int i = 0; i < m*n; i++){
		a[i] = fmod(rand(), 10.);
	}
	return a;
}

double*sumMatrix(int m, int n, double *a, double *b){
	double *c = createMatrix(m, n);

	for (int i = 0; i < m*n; i++){
		c[i] = a[i] + b[i];
	}
	return c;
}

double*prodMatrix(int m1, int n1, int m2, int n2, double *a, double *b){
	if (n1 != m2){
		return NULL;
	}
	double *c = createMatrix(m1, n2);

	int i, j, k;
	double sum;

	for (i = 0; i < m1; i++){
		for (k = 0; k < n2; k++){
			sum = 0.;
			for (j = 0; j < n1; j++){
				//c[i][k] = c[i][k] + a1[i][j] * a2[j][k];
				sum += a[i*n1 + j] * b[j*n2 + k];
			}
			c[i*n2 + k] = sum;
		}
	}

	return c;
}

string matrixToString(int m, int n, double *a){
	string s = "";

	//int v = 0;

	for (int i = 0; i < m*n; i++){
		s = s + to_string(a[i])+" ";
		//v++;
		if (((i+1)%n) == 0){
			s = s + "\n";
		}
	}

	return s;
}

void deleteMatrix(double *a){
	delete[] a;
}

void printMatrix(string title, int m, int n, double *a){
	cout << title << "\n";
	cout << matrixToString(m, n, a)<<"\n";
}

int main(void){

	int m, n;
	m = 2;
	n = 2;

	srand((unsigned int)time(NULL));

	double *a = createMatrix(m, n);
	double *b = createMatrix(m, n);

	printMatrix("matrix a", m, n, a);
	printMatrix("matrix b", m, n, b);

	double *c = sumMatrix(m, n, a, b);

	printMatrix("a+b", m, n, c);

	double *d = prodMatrix(m, n, m, n, a, b);

	printMatrix("ab", m, n, d);

	deleteMatrix(a);
	deleteMatrix(b);
	deleteMatrix(c);
	deleteMatrix(d);

	return 0;
}